# Harsh Singh - Portfolio

[![LinkedIn](https://img.shields.io/badge/LinkedIn-Connect-blue)](https://www.linkedin.com/in/harshsingh-io)
[![Website](https://img.shields.io/badge/Website-Visit%20Portfolio-brightgreen)]([https://www.harshsingh.com](https://harshsingh-io.github.io/portfolio/))
[![GitHub](https://img.shields.io/badge/GitHub-Follow-ff69b4)](https://github.com/harshsingh-io)

## About Me
I am a software engineering student at RGPV University with a passion for coding and problem-solving. My goal is to make a meaningful impact through technology and innovation. Here's a brief overview of my skills, education, and projects.

## Education
- **Bachelor of Technology**
  - *RGPV University*, Bhopal, MP, India
  - *07/2021 - 06/2025 (Expected)*
  - *Major: Computer Science and Engineering*
  - *GPA: 7.84/10*

## Skills
- **Programming Languages / Databases:** Java, Kotlin, SQL, C Programming
- **Tools and Technologies:** RESTful APIs, Firebase, Realtime Database, Git, GitHub, JSON, Android Studio, Android SDK, XML, Linux, and more
- **Technical Proficiencies:** Data Structures and Algorithms, Object-Oriented Programming, Database Management Systems, Computer Networking
- **Soft Skills:** Problem Solving, Teamwork, Effective Communication, Creativity, Leadership

## Projects

### KanbanBoard - Project Management App
- *Link:* [GitHub Repository](https://github.com/harshsingh-io/KanbanBoard)
- *Duration:* 06/2023 - Present
- *Description:* A project management application that uses Firebase Firestore to store data in the cloud. The application allows users to create projects, add tasks, and supports drag-and-drop functionality, notifications, and user profiles.
- *Technologies Used:* Android, Java, Firebase Firestore
- *Skills Demonstrated:* Firestore integration, Drag-and-Drop, Notifications

### EcoSync - Water and Electricity Tracking App
- *Link:* [GitHub Repository](https://github.com/harshsingh-io/EcoSync)
- *Duration:* 09/2023 - 09/2023
- *Description:* Developed an innovative app for real-time monitoring and sustainable electricity usage. Achieved 95% data accuracy, reduced utility bills by 20%, and decreased resource consumption by 15%.
- *Technologies and Tools:* Android Studio, Kotlin, Node.js, Firebase, MongoDB
- *Hardware Used:* NodeMCU, LCD, Voltage and Current Sensor

## Awards And Honors
- **First Prize, CodeEnergia 2023 Hackathon**
  - *Link:* [Hackathon Details]([https://www.hackathon-link.com](https://drive.google.com/file/d/1LlTnIn35ww6L6eBM40ae3QIM9F3puYFN/view?usp=sharing))
  - *09/2023*
  - Led the winning team in developing "EcoSync - Water and Electricity Tracking App."
  - Collaborated with a diverse team, showcasing strong leadership and teamwork.
  - Recognized for innovation and received post-hackathon support.

## Experience
- **Android Lead, Google Developer Student Clubs**
  - *Bhopal, MP, India*
  - *08/2023 - Present*
  - Successfully executed 3 tech events with 550+ student participants.
  - Managed a team of 10 volunteers, increasing club membership by 500+ within a week.

- **Open-Source Contributor, Hacktoberfest 2022**
  - *Remote*
  - *10/2022 - 11/2022*
  - Contributed to "TheAlgorithms" repository and improved open-source projects' performance.
  - Fixed 15 bugs, including UI and Coroutines enhancements.

## Certifications
- **Data Structures and Algorithms with Java | Apna College**
  - *Link:* [Certification Details]([https://www.certification-link.com](https://drive.google.com/file/d/1WM-JX7G3dJQckX75zrg9DWdKJYPTGFkC/view))
  - *08/2022 - 02/2023*

- **AWS Essentials Certification | ITE Infotech**
  - *Link:* [Certification Details]([https://www.certification-link.com](https://drive.google.com/file/d/179rpQh7VVHRHRfN_jDyUxhzqJ8PgfTqP/view?usp=drivesdk))
  - *10/2022 - 10/2022*

## Contact Me
- **Email:** harshsingh704888@gmail.com
- **Phone:** +91 70472 72436
